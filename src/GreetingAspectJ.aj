public aspect GreetingAspectJ{
	long startTime;
	long endTime;
	
	//Se crea un pointcut para que llame al m�todo de greeting
	pointcut callGreeting(): call(* HelloAspectJDemo.greeting());
	
	//Se registra el Joint Point antes de que se llame al m�todo
	before() : callGreeting() {
		startTime = System.nanoTime();
	}
	
	//Se registra el Joint Point despu�s de que se llame al m�todo y se imprime
	after() : callGreeting() {
		endTime = System.nanoTime() - startTime;
		System.out.println();
		System.out.println();
		System.out.println("Execution time after greeting: "+endTime+" ns");
	}
}